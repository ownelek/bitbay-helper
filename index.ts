import * as fs from 'fs';

interface RawOldTransaction {
    timestamp: string;
    ignoredTimestamp: string;
    label: string;
    value: string;
    balanceAfter: string;
}

interface Transaction {
    timestamp: string;
    label: string | TransactionLabel;
    value: number;
    currency: string;
    balanceAfter: number;
}

enum TransactionLabel {
    WalletIn = 'Wpłata na rachunek',
    WalletOut = 'Wypłata środków',
    TransactionIn = 'Otrzymanie środków z transakcji na rachunek',
    TransactionOut = 'Pobranie środków z transakcji z rachunku',
    TransactionFee = 'Pobranie prowizji za transakcję'
}

interface TransactionInfo {
    timestamp: Date;
    currency: string;
    baseValue: number;
    price: number;
    amount: number;
}

function fixTimestamp(timestamp: string): string {
    let parts: Array<string> = timestamp.split(' ');

    return [parts[0].split('-').reverse().join('-'), parts[1]].join(' ');
}

function fixLabel(label: string): string {
    switch(label) {
        case 'Otrzymanie środków': return 'Wpłata na rachunek';
        case 'Zewnętrzny przelew przychodzący': return 'Wpłata na rachunek';

        case 'Zakup waluty': return 'Otrzymanie środków z transakcji na rachunek';
        case 'Zapłata za zakup waluty': return 'Pobranie środków z transakcji z rachunku';
        case 'Prowizja od transakcji': return 'Pobranie prowizji za transakcję';
        case 'Wypłata środków na konto': return 'Wypłata środków';
        case 'Przelew środków': return 'Wypłata środków';
        default: return label;
    }
}

function readCsvFile(filepath: string): Array<Transaction> {
    return fs.readFileSync(filepath, { encoding: 'utf8' })
        .split('\n')
        .map(rawString => rawString.split(';'))
        .map(rawTransaction => {

            return {
                timestamp: fixTimestamp(rawTransaction[0]),
                label: rawTransaction[1],
                value: parseFloat(rawTransaction[2]),
                currency: rawTransaction[3],
                balanceAfter: parseFloat(rawTransaction[4])
            };
        });
}

function readOldCsvReport(filepath: string): Array<Transaction> {
    return fs.readFileSync(filepath, { encoding: 'utf8' })
        .split('\n')
        .map(rawString => rawString.split(';'))
        .map(rawTransaction => ({
            timestamp: fixTimestamp(rawTransaction[0]),
            label: fixLabel(rawTransaction[2]),
            value: parseFloat(rawTransaction[3].split(' ')[0].replace(',', '')),
            currency: rawTransaction[3].split(' ')[1],
            balanceAfter: parseFloat(rawTransaction[4].split(' ')[0].replace(',', ''))
        }));
}

function saveFile(filepath: string, fileContent: string): void {
    fs.writeFileSync(filepath, fileContent);
}

function runConversion(): void {
    let oldReportTransactions: Array<Transaction> = readOldCsvReport('./reports/report_2018-04-20 23-50-25.csv');

    let newReportCsv: string = oldReportTransactions.map(
        transaction => `${transaction.timestamp};${transaction.label};${transaction.value};${transaction.currency};${transaction.balanceAfter}`
    ).join('\n');

    saveFile('./newreport.csv', newReportCsv)
}

function checkIntegrity(): void {
    let transactions: Array<Transaction> = readCsvFile('./reports/bitbay2017history.csv');
    transactions = transactions.reverse();

    for(let i: number = 0; i < transactions.length; i++) {
        let transaction: Transaction = transactions[i];

        if(transaction.label === TransactionLabel.TransactionIn && transactions[i + 1].label !== TransactionLabel.TransactionOut) {
            console.log('Not integral at transaction', transactions.length - i);
            break;
        }
    }
}

function summarizeLastYear(): void {
    let transactions: Array<Transaction> = readCsvFile('./reports/bitbay2017history.csv');
    transactions = transactions.reverse();

    let buys: Map<string, Array<TransactionInfo>> = new Map();
    let sells: Map<string, Array<TransactionInfo>> = new Map();
    let feesPaid: Map<string, number> = new Map();
    let totalInputs: Map<string, number> = new Map();
    let totalOutputs: Map<string, number> = new Map();

    for(let i: number = 0; i < transactions.length; i++) {
        let transaction: Transaction = transactions[i];
        if(transaction.label === TransactionLabel.WalletIn) {
            let currentInputs: number = totalInputs.get(transaction.currency);

            if(!currentInputs) {
                totalInputs.set(transaction.currency, currentInputs = 0);
            }

            totalInputs.set(transaction.currency, currentInputs + transaction.value);
        } else if(transaction.label === TransactionLabel.WalletOut) {
            let currentOutputs: number = totalOutputs.get(transaction.currency);

            if(!currentOutputs) {
                totalOutputs.set(transaction.currency, currentOutputs = 0);
            }

            totalOutputs.set(transaction.currency, currentOutputs + transaction.value);
        } else if(transaction.label === TransactionLabel.TransactionIn) {
            if(transaction.currency === 'PLN') {
                // if TransactionIn is PLN then we sell crypto
                let outTransaction: Transaction = transactions[i + 1];
                let price: number = Math.abs(transaction.value / outTransaction.value);
                let currentSells: Array<TransactionInfo> = sells.get(outTransaction.currency) || [];
                currentSells.push({
                    timestamp: new Date(transaction.timestamp),
                    currency: outTransaction.currency,
                    baseValue: transaction.value,
                    price: price,
                    amount: Math.abs(outTransaction.value)
                });
                sells.set(outTransaction.currency, currentSells);
            } else {
                // if not PLN then we buy crypto
                let outTransaction: Transaction = transactions[i + 1];
                let price: number = Math.abs(outTransaction.value / transaction.value);
                let currentBuys: Array<TransactionInfo> = buys.get(transaction.currency) || [];
                currentBuys.push({
                    timestamp: new Date(transaction.timestamp),
                    currency: transaction.currency,
                    baseValue: Math.abs(outTransaction.value),
                    price: price,
                    amount: transaction.value
                });
                buys.set(transaction.currency, currentBuys);
            }
        }
    }

    console.log("Total inputs:");
    for(let [currency, value] of totalInputs.entries()) {
        console.log(currency, value);
    }

    console.log("Total outputs:");
    for(let [currency, value] of totalOutputs.entries()) {
        console.log(currency, value);
    }

    console.log("Total Balance:");
    let currentBalance: Map<string, number> = new Map();

    for(let [currency, value] of totalInputs.entries()) {
        let currentCurrencyBalance: number = currentBalance.get(currency);

        if(!currentCurrencyBalance) {
            currentBalance.set(currency, currentCurrencyBalance = 0);
        }

        currentBalance.set(currency, currentCurrencyBalance + value);
    }

    for(let [currency, value] of totalOutputs.entries()) {
        let currentCurrencyBalance: number = currentBalance.get(currency);

        if(!currentCurrencyBalance) {
            currentBalance.set(currency, currentCurrencyBalance = 0);
        }

        currentBalance.set(currency, currentCurrencyBalance + value);
    }

    for(let [currency, value] of currentBalance.entries()) {
        console.log(currency, value);
    }

    // console.log(`Total buys value: (total buys: ${buys.length})`);
    // console.log(`${buys.reduce((sum: number, transactionInfo: TransactionInfo) => sum + transactionInfo.baseValue, 0)} PLN`);
    //
    // console.log(`Total sells value: (total sells ${sells.length})`);
    // console.log(`${sells.reduce((sum: number, transactionInfo: TransactionInfo) => sum + transactionInfo.baseValue, 0)} PLN`);

    let currencies: Set<string> = new Set();
    Array.from(sells.keys()).forEach(currencies.add.bind(currencies));
    Array.from(buys.keys()).forEach(currencies.add.bind(currencies));
    let profits: Array<number> = [];

    for(let currency of currencies) {
        console.log(`Currency: ${currency}`);
        let sellIndex: number = 0;
        let buyIndex: number = 0;
        let currentSells: Array<TransactionInfo> = sells.get(currency) || [];
        let currentBuys: Array<TransactionInfo> = buys.get(currency) || [];
        printTradeTable(currentBuys, currentSells);

        while(sellIndex < currentSells.length && buyIndex < currentBuys.length) {
            let currentBuy: TransactionInfo = currentBuys[buyIndex];
            let currentSell: TransactionInfo = currentSells[sellIndex];
            let profit: number = 0;

            if(currentBuy.amount < currentSell.amount) {
                let proportionalBaseValue: number = currentSell.baseValue * (currentBuy.amount / currentSell.amount);
                profit += proportionalBaseValue - currentBuy.baseValue;
                buyIndex++;
                currentSell.baseValue -= proportionalBaseValue;
                currentSell.amount -= currentBuy.amount;
                console.log(
                    `Sold ${roundTo(currentBuy.amount, 10)} ${currency} at ${roundTo(currentSell.price, 2)} `
                    + `(bought at ${roundTo(currentBuy.price, 2)}). Profit: ${roundTo(profit, 2)} PLN `
                    + `(${roundTo(currentSell.baseValue, 2)}, ${roundTo(proportionalBaseValue, 2)}, ${roundTo(currentBuy.baseValue, 2)})`
                );
            } else {
                let proportionalBaseValue: number = currentBuy.baseValue * (currentSell.amount / currentBuy.amount);
                profit += currentSell.baseValue - proportionalBaseValue;
                sellIndex++;
                currentBuy.baseValue -= proportionalBaseValue;
                currentBuy.amount -= currentSell.amount;
                console.log(
                    `Sold ${roundTo(currentSell.amount, 10)} ${currency} at ${roundTo(currentSell.price, 2)}. `
                    + `(bought at ${roundTo(currentBuy.price, 2)}). Profit: ${roundTo(profit, 2)} PLN `
                    + `(${roundTo(currentSell.baseValue, 2)}, ${roundTo(proportionalBaseValue, 2)}, ${roundTo(currentBuy.baseValue, 2)})`
                );
            }

            profits.push(profit);
        }


        console.log('Remaining buys: ', currentBuys.slice(buyIndex));
        console.log('Remaining sells: ', currentSells.slice(sellIndex));
    }

    console.log(`Total profit from transactions: (total profits: ${profits.length})`);
    console.log(`${profits.reduce((sum: number, profit: number) => sum + profit, 0)} PLN`);
}

function printTradeTable(buys: Array<TransactionInfo>, sells: Array<TransactionInfo>): void {
    const FILL_COUNTER: number = 32;
    console.log('*'.repeat(FILL_COUNTER * 2 + 4));
    console.log(`*${padEnd(' Buys', FILL_COUNTER)}**${padEnd(' Sells', FILL_COUNTER)}*`);
    console.log('*'.repeat(FILL_COUNTER * 2 + 4));
    let length: number = Math.max(buys.length, sells.length);
    for(let i: number = 0; i < length; i++) {
        let buy: TransactionInfo = buys[i];
        let sell: TransactionInfo = sells[i];
        console.log(
            `*${buy ? padEnd(` ${padEnd(roundTo(buy.amount, 8).toString(), 10)} | ${roundTo(buy.price, 2)}`, FILL_COUNTER) : ' '.repeat(FILL_COUNTER)}*`
            + `*${sell ? padEnd(` ${padEnd(roundTo(sell.amount, 8).toString(), 10)} | ${roundTo(sell.price, 2)}`, FILL_COUNTER) : ' '.repeat(FILL_COUNTER)}*`
        );
    }
    console.log('*'.repeat(FILL_COUNTER * 2 + 4));
}

function padEnd(s: string, n: number, c: string = ' '): string {
    return s + c.repeat(n - s.length);
}

function roundTo(n: number, precision: number): number {
    let multi: number = 10 ** precision;
    return Math.round(n  * multi) / multi;
}

summarizeLastYear();